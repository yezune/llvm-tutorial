.PHONY: docker run clean


docker:
	docker build --tag yezune/llvm-tutorial ./docker

run:
	docker run -it --rm --volume ${PWD}/tutorial:/tutorial --workdir=/tutorial --hostname llvm-tutorial yezune/llvm-tutorial

#clean:
#	docker image rm yezune/llvm-tutorial

.PHONY: clone config build install uninstall clean all

clone:
	git clone https://github.com/llvm/llvm-project.git llvm-project

SOURCE_DIR=./llvm-project/llvm
BUILD_DIR=./build
INSTALL_PREFIX=/opt
#PROJECTS="clang;clang-tools-extra;cross-project-tests;libc;libclc;lld;lldb;openmp;polly;pstl"
#RUNTIMES="compiler-rt;libc;libcxx;libcxxabi;libunwind;openmp"
PROJECTS="clang;clang-tools-extra"
RUNTIMES="compiler-rt;libc;libcxx;libcxxabi;libunwind;openmp"
RUNTIMES="libcxx;libcxxabi"
BUILD_TYPE=Release
BUILD_SYSTEM="Unix Makefiles"

config:
	cmake -S ${SOURCE_DIR} -B ${BUILD_DIR} -G ${BUILD_SYSTEM} -DLLVM_ENABLE_PROJECTS=${PROJECTS} -DLLVM_ENABLE_RUNTIMES=${RUNTIMES} -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DCMAKE_BUILD_TYPE=${BUILD_TYPE}

build:
	cd build && make -j

install:
	cd build && make install

uninstall:
	cd build && xargs rm < install_manifest.txt

clean:
	rm -rf build

all: cmake build install
