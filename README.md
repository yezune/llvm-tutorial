# LLVM Tutorial on Docker - Kaleidoscope.

- The Origin is from [LLVM-Tutorial Series](https://llvm.org/docs/tutorial/index.html)

## How to make executable file and run it. 
```sh
# 1. Copy this tutorial from Gitlab
$ git clone https://gitlab.com/yezune/llvm-tutorial.git 

# 2. Get or Make docker image
$ cd llvm-tuturial

# if you want to make docker image.
$ make docker 

# download docker image and run it.
$ make run 

root@llvm-tutorial:/tutorial#
root@llvm-tutorial:/tutorial# make all # To compile sources of all chapters
root@llvm-tutorial:/tutorial# cd build
root@llvm-tutorial:/tutorial/build# ./ch01
```

Let's go to [LLVM Tutorial](tutorial/)
  
